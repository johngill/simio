# simio

A small lightweight library to support co-simulation of HDL.

Using System Verilog's DPI interface and the ucontext library, a simple co-routine mechanism is created to pass data between SV and C.