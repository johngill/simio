#ifndef SIMIO_HEADER_H
#define SIMIO_HEADER_H

#include <stdlib.h>
#include <stdint.h>
#include <ucontext.h>
#include <svdpi.h>

// List of co-routine simulation io interfaces supported
#define SIMIO_SIM               0  // reserved for the RTL simulation
#define SIMIO_IF_HOST_WITH_PCIE 1 
#define SIMIO_IF_HOST_WITH_AXI  2 
#define SIMIO_IF_S_AXIS         3 
#define SIMIO_IF_M_AXIS         4 
#define SIMIO_IF_SM_AXIS        5 
#define SIMIO_IF_10GBE_TX       6 
#define SIMIO_IF_10GBE_RX       7 
#define SIMIO_IF_10GBE_TXRX     8 
#define SIMIO_IF_M_AXI_WR       9 
#define SIMIO_IF_M_AXI_RD       10
#define SIMIO_IF_M_AXI_WRRD     11
#define SIMIO_IF_S_AXI_WR       12
#define SIMIO_IF_S_AXI_RD       13
#define SIMIO_IF_S_AXI_WRRD     14


typedef struct dpi_cfg {
    char              fnslib_str[256]; // shared library path + fname
    char              fnmain_str[256]; // entry point of user defined main fn
    uint32_t          v2c_size;
    uint32_t          c2v_size;
} dpi_cfg_t;

typedef struct coroutine {
    struct coroutine *next;
    void             *ss_sp;
    ucontext_t        uctx;
    dpi_cfg_t         dpi_cfg;
    uint32_t          dpi_cmd;
    uint32_t          dpi_id;
    void             *dl_handle;
    void            (*fn_main)(uint32_t);
    uint32_t          buf_c2v[128];
    uint32_t          buf_v2c[128];
} coroutine_t;

// Globals
coroutine_t *crlist = NULL;
ucontext_t   SimCtx;

// private functions
char        *simio_strrev(char *str);
unsigned     simio_roundup(unsigned num, unsigned align);
uint32_t     simio_coroutine_id(void);
coroutine_t *simio_coroutine(const uint32_t dpi_id);
void         simio_handle_error(const char *simio_str);
void        *simio_alloc_stack(const unsigned stksize);
void         simio_create_ucontext(coroutine_t *pcr);
void         simio_open_libsimio (coroutine_t *pcr);
void         simio_dump(const uint32_t dpi_id);

#endif
