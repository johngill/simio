`timescale 1ps/1ps

`define SIMIO_IF_XCHG   1
`define SIMIO_IF_FINISH 2
`define SIMIO_IF_ERROR  3

module simio #(parameter tcq = 1) 
   (input  clk,
    input  reset );

   localparam nchars = 256;
   // To simplify handling data in the C-domain we restrict ourselves to 2-state values
   // Preferring to use bit[] rather than logic[]

   typedef struct packed {
      bit [31:0]         c2v_size;
      bit [31:0]         v2c_size;
      bit [nchars*8-1:0] fnmain_str;
      bit [nchars*8-1:0] fnslib_str;
   } dpi_t;

   typedef struct packed {
      bit [31:0]  ready;
      bit [31:0]  reset;
   } v2c_t;

   typedef struct packed {
      bit [31:0] data;
      bit [31:0] valid;
   } c2v_t;

   dpi_t      dpi_cfg; 
   v2c_t      v2c;
   c2v_t      c2v;
   bit [31:0] dpi_id;
   bit [31:0] dpi_cmd;


   assign dpi_cfg.fnslib_str = "./dpi/libsimio.so";
   assign dpi_cfg.fnmain_str = "simio_maxis_main";
   assign dpi_cfg.v2c_size   = 8;
   assign dpi_cfg.c2v_size   = 8;

   import "DPI-C" context function void simio_open      (input  dpi_t      dpi_cfg, output bit [31:0] dpi_id);
   import "DPI-C" context function void simio_close     (input  bit [31:0] dpi_id);
   import "DPI-C" context function void simio_yield_v2c (input  bit [31:0] dpi_id);
   import "DPI-C" context function void simio_shmem     (input  bit [31:0] dpi_id,
                                                         input  v2c_t      v2c,
		                                         output bit [31:0] dpi_cmd,
		                                         output c2v_t      c2v );

   initial begin
      simio_open(dpi_cfg, dpi_id);
   end

   always @(*) begin
      simio_yield_v2c(dpi_id);
      simio_shmem(dpi_id, v2c, dpi_cmd, c2v);
   end 

   always @(posedge clk) begin
//      simio_yield_v2c(dpi_id);
//      simio_shmem(dpi_id, v2c, dpi_cmd, c2v);

      if (dpi_cmd == `SIMIO_IF_XCHG) begin
	 //$display("V: %m: lfsr64_r=%h valid=%x data=%x ready=%x", lfsr64_r, c2v.valid, c2v.data, v2c.ready);
      end else begin
	 if (dpi_cmd == `SIMIO_IF_FINISH) begin
	    $display("%m: Simulation finishing - initiated from DPI-C");
	 end else if (dpi_cmd == `SIMIO_IF_ERROR) begin
	    $display("%m: Simulation Error - initiated from DPI-C");
	 end
	 simio_close(dpi_id);
	 $finish();
      end
   end

       
   bit   [31:0]  cyclecnt, cyclecnt_r;
   bit   [63:0]  lfsr64, lfsr64_r;

   assign        cyclecnt  = cyclecnt_r + 32'h1;
   assign        lfsr64    = {lfsr64_r[62:0], lfsr64_r[63] ^ lfsr64_r[62] ^ lfsr64_r[60] ^ lfsr64_r[59]};
   assign        v2c.ready = lfsr64_r[63];
   assign        v2c.reset = reset;

   initial begin
      cyclecnt_r = 0;
      // possibly initialise RX signals from c2v
   end


   always @(posedge clk) begin

      cyclecnt_r <= #tcq cyclecnt;

      if (reset == 1'b1) begin
	 lfsr64_r <= #tcq 64'hFEED_FACE_DEAD_BEEF;
      end else begin
	 lfsr64_r <= #tcq lfsr64;
      end
   end

endmodule
