`timescale 1ps/1ps

module simio_tb();
   logic clk   = 0;
   logic reset = 1;

   always begin
      #5000 clk = 1;
      #5000 clk = 0;
   end

   initial begin
      #10000 reset = 0;
   end

   simio simio_i (
     .clk    (clk),
     .reset  (reset) );

endmodule
