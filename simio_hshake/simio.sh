#!/bin/sh

function check_cmd {    
    if [ $? -ne 0 ]; then
	echo $1
	exit 1
    fi
}

src=$PWD

if [ -d "build" ]; then rm -rf build; fi
mkdir -p build/dpi
check_cmd "Failed to create build dir" 

# DPI COMPILATION
#xsc -C gcc -v 1 -i ../simio.c -i ../simio_maxis_main.c
#xsc -C gcc -v 1 -i ../simio.c
#xsc -compile ../simio.c ../simio_maxis_main.c -work xsc
#xsc -link    xsim.dir/xsc/simio.lnx64.o xsim.dir/xsc/simio_maxis_main.lnx64.o -work xsc
make
check_cmd "\tFailed to compile DPI: $0"

cd build
# RTL COMPILATION
xvlog -sv $src/simio.sv
xvlog -sv $src/simio_tb.sv
check_cmd "\tFailed to compile HDL: $0"

#ELABORATION
xelab -debug typical simio_tb -L unisim -L secureip -sv_root ${src}/build/dpi -sv_lib dpi 
check_cmd "\tFailed to elaborate HDL: $0"

#SIMULATION
if [[ "$1" == "-gui" ]]; then
    xsim -gui simio_tb
else 
    xsim -runall simio_tb
fi
