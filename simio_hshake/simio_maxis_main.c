#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <svdpi.h>

#include "simio_public.h"

typedef struct shmem_c2v {
    svBitVecVal maxis_valid;
    svBitVecVal maxis_data;    
} shmem_c2v_t;

typedef struct shmem_v2c {
    svBitVecVal reset;
    svBitVecVal maxis_ready;
} shmem_v2c_t;

void
simio_shmem_maxis_init(shmem_c2v_t *shmem_c2v, shmem_v2c_t *shmem_v2c)
{
    shmem_c2v->maxis_valid = 0;
    shmem_c2v->maxis_data  = 0xfeedface;
    
    shmem_v2c->reset       = 1;
    shmem_v2c->maxis_ready = 0;
}

// Entry point for user code
void
simio_maxis_main (uint32_t dpi_id)
{
    static unsigned    cycle     = 0;
    static unsigned    data      = 0;
    shmem_c2v_t       *shmem_c2v = (shmem_c2v_t *)simio_shmem_c2v(dpi_id);
    shmem_v2c_t       *shmem_v2c = (shmem_v2c_t *)simio_shmem_v2c(dpi_id);
    unsigned           dbg       = 0;

    simio_shmem_maxis_init(shmem_c2v, shmem_v2c);

    if (dbg) {
        printf("C: simio_maxis_main: dpi_id=%x shmem_c2v=%p shmem_v2c=%p\n", dpi_id, shmem_c2v, shmem_v2c); 
        fflush(NULL);
    }
   
    while (1) {
        if (shmem_v2c->reset == 1) {
            shmem_c2v->maxis_valid = 0;
            shmem_c2v->maxis_data  = rand() & 0xffffffff;
        } else {
            shmem_c2v->maxis_valid = 1;
            shmem_c2v->maxis_data  = data;            
        }
        simio_yield_c2v(dpi_id, SIMIO_CMD_XCHG);
        
        if (shmem_c2v->maxis_valid && shmem_v2c->maxis_ready) {
            data = data + 1;
        }

        if (cycle > (256)){// * 1024)) {
            printf("C: PCIe Main Cycle=%d\n", cycle); fflush(NULL);            
            simio_yield_c2v(dpi_id, SIMIO_CMD_FINISH);
        }
        else if (dbg) {
            printf("C: PCIe Main Cycle=%d\n", cycle); 
            fflush(NULL);            
        }
        cycle = cycle + 1;
    }
}
