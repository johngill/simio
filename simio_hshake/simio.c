#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ucontext.h>
#include <svdpi.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <sys/mman.h>

#include "simio_private.h"
#include "simio_public.h"

char *
simio_strrev(char *str)
{
    unsigned len = (unsigned)strlen(str);
    unsigned i;

    for (i = 0; i < (len >> 1); i++) {
        char s          = str[i];        
        str[i]          = str[len - 1 - i];
        str[len - 1 -i] = s;
    }

    return str;
}

unsigned
simio_roundup(unsigned num, unsigned align)
{
    return ((num + align - 1) & ~(align - 1));
}

uint32_t
simio_coroutine_id (void)
{
    static uint32_t dpi_id = 0xfe;
    unsigned        dbg  = 0;

    if (dbg) {
       printf("C: simio_coroutine_id: %u\n", dpi_id); 
       fflush(NULL);
    }

    return dpi_id++;
}

coroutine_t *
simio_coroutine(const uint32_t dpi_id)
{
    coroutine_t *pcr = crlist;

    while (pcr) {
        if (pcr->dpi_id == dpi_id)
            return pcr;
    }

    printf("C: simio_coroutine: coroutine instance = %d: Does not exist. ERROR\n", dpi_id);fflush(NULL);
    exit(EXIT_FAILURE);
}

void
simio_handle_error(const char *simio_str) 
{
    perror(simio_str); 
    exit(EXIT_FAILURE); 
}

void
simio_yield_v2c(const svBitVecVal *dpi_id)
{
    coroutine_t *pcr = simio_coroutine(*dpi_id);
    unsigned     dbg = 0;

    if (dbg) {printf("C: simio_yield_v2c exit\n"); fflush(NULL);}

    if (swapcontext(&SimCtx, &pcr->uctx) == -1)
        simio_handle_error("swapcontext_v2c");

    if (dbg) {printf("C: simio_yield_v2c entry\n"); fflush(NULL);}
}

void
simio_yield_c2v(const uint32_t dpi_id, const uint32_t dpi_cmd)
{
    coroutine_t *pcr = simio_coroutine(dpi_id);
    unsigned     dbg = 0;

    if (dbg) {printf("C: simio_yield_c2v exit\n"); fflush(NULL);}

    pcr->dpi_cmd = dpi_cmd;
    if (swapcontext(&pcr->uctx, &SimCtx) == -1)
        simio_handle_error("swapcontext_c2v");

    if (dbg) {printf("C: simio_yield_c2v entry\n"); fflush(NULL);}
}

void *
simio_alloc_stack(unsigned stk_size)
{
    unsigned page_size  = sysconf(_SC_PAGE_SIZE);
    unsigned alloc_size = simio_roundup(stk_size, page_size) + (2 * page_size);
    int      fd;
    char    *ptr;
    unsigned dbg = 1;

    if ((fd = open("/dev/zero", O_RDWR)) < 0)
        return NULL;

    ptr = (char *) mmap(NULL, alloc_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
    if (ptr == MAP_FAILED) {
        close(fd);
        return NULL;
    }
    close(fd);

    if (dbg)
        printf("C: simio_alloc_stack: alloc_size=%x page_size=%x ptr=%p ptr+page_size=%p\n", 
               alloc_size, page_size, ptr, ptr+page_size);

    mprotect (ptr, page_size, PROT_NONE);
    mprotect (ptr + page_size + simio_roundup(stk_size, page_size), page_size, PROT_NONE);

    return (void *) (ptr + page_size);
}

void
simio_create_ucontext(coroutine_t *pcr)
{
    unsigned ssize = 8 * 1024;

    if (getcontext(&pcr->uctx) == -1) 
        abort();    

    pcr->ss_sp                 = simio_alloc_stack(ssize);
    pcr->uctx.uc_link          = &SimCtx;
    pcr->uctx.uc_stack.ss_sp   = pcr->ss_sp;
    pcr->uctx.uc_stack.ss_size = ssize;
    makecontext (&pcr->uctx, (void (*) (void)) pcr->fn_main, 1, pcr->dpi_id);    
}

void
simio_open_libsimio (coroutine_t *pcr)
{
    void *dl_handle;
    char *error;
    char *dl_str;
    char  cwd[256];

    if (getcwd(cwd, sizeof(cwd)) != NULL)
        printf("C: %s: CWD = %s\n", __FUNCTION__, cwd);
    else
        perror("getcwd() error");

    if (strcmp(pcr->dpi_cfg.fnslib_str, "") == 0) {
        dl_str = NULL;
    } else {
        dl_str = pcr->dpi_cfg.fnslib_str;
    }
    dl_handle = dlopen(dl_str, RTLD_NOW); 
    if (! dl_handle) {
        printf("C: FAILED: dlopen: %s\n", dlerror());       
    }
    printf("C: simio_open_libsimio: dl_handle=%p\n", dl_handle);fflush(NULL);
    dlerror(); // clr old errors
    *(void **)(&pcr->fn_main) = dlsym(dl_handle, pcr->dpi_cfg.fnmain_str);
    if ((error = dlerror()) != NULL) {
        printf("C: FAILED: dlsym: %s\n", error);
        exit(EXIT_FAILURE);
    }
    pcr->dl_handle = dl_handle;
    printf("C: open_libsimio: fnslib_str=%s fnmain_str=%s fn_main=%p\n", 
           pcr->dpi_cfg.fnslib_str, pcr->dpi_cfg.fnmain_str, pcr->fn_main);fflush(NULL);
}

uint32_t *
simio_shmem_c2v(const uint32_t dpi_id)
{
    coroutine_t *pcr = simio_coroutine(dpi_id);
    return pcr->buf_c2v;
}

uint32_t *
simio_shmem_v2c(const uint32_t dpi_id)
{
    coroutine_t *pcr = simio_coroutine(dpi_id);
    return pcr->buf_v2c;
}

void
simio_shmem(const svBitVecVal *dpi_id, const svBitVecVal *v2c, svBitVecVal *dpi_cmd, svBitVecVal *c2v)
{
    coroutine_t       *pcr       = simio_coroutine(*dpi_id);        
    uint32_t           c2v_size  = pcr->dpi_cfg.c2v_size;
    uint32_t           v2c_size  = pcr->dpi_cfg.v2c_size;
    unsigned           dbg       = 0;

    if (dbg) {
        printf("C: crlist=%p pcr=%p next=%p uctx=%p dpi_id=%d\n", crlist, pcr, pcr->next, &pcr->uctx, pcr->dpi_id); 
        printf("C: v2c[0]=%08x v2c[1]=%08x\n", v2c[0], v2c[1]);fflush(NULL);
        fflush(NULL);
    }

    // Set the Command
    *dpi_cmd = pcr->dpi_cmd;
    
    // Copy shared memory data from C coroutine to SV data outputs
    memcpy((void *)c2v,          (void *)pcr->buf_c2v, (size_t)c2v_size);
    memcpy((void *)pcr->buf_v2c, (void *)v2c,          (size_t)v2c_size);
}

void
simio_dbg_pid(void)
{
    pid_t    pid;
    unsigned dbg = 0;
    
    if ((pid = getpid()) < 0) {
        perror("C: Unable to get pid\n");
    } else {
        printf("C: pid=%d\n", pid);
    }
    if (dbg) 
        sleep(10);
    fflush(NULL);    
}

void
simio_dump(const uint32_t dpi_id)
{
    coroutine_t *pcr = simio_coroutine(dpi_id);
    int          i;

    printf("C: next=%p fn_main=%p dpi_id=%08x dpi_cmd=%08x\n", pcr->next, pcr->fn_main, pcr->dpi_id, pcr->dpi_cmd);
    for (i = 0; i < 128; i++) {
        printf("C: v2c[%02d]=%08x c2v[%02d]=%08x\n", i, pcr->buf_v2c[i], i, pcr->buf_c2v[i]);
    }
    fflush(NULL);

}

void
simio_open(svBitVecVal *dpi_cfg, svBitVecVal *dpi_id)
{    
    coroutine_t *pcr = (coroutine_t *)malloc(sizeof(coroutine_t));
    dpi_cfg_t   *cfg = (dpi_cfg_t *)dpi_cfg;

    memset(pcr, 0, sizeof(coroutine_t));
    pcr->dpi_id    = simio_coroutine_id();
    pcr->next      = NULL;
    
    simio_strrev(cfg->fnslib_str);
    simio_strrev(cfg->fnmain_str);
    printf("C: simio_open: shared_lib=%s entry_point=%s\n", cfg->fnslib_str, cfg->fnmain_str);
    fflush(NULL);

    sprintf(pcr->dpi_cfg.fnslib_str, cfg->fnslib_str);
    sprintf(pcr->dpi_cfg.fnmain_str, cfg->fnmain_str);
    pcr->dpi_cfg.v2c_size = cfg->v2c_size;
    pcr->dpi_cfg.c2v_size = cfg->c2v_size;
    simio_open_libsimio(pcr);    
    simio_dbg_pid();
    
    if (crlist == NULL) {
        crlist    = pcr;
    } else {
        pcr->next = crlist;
        pcr       = crlist;
    }

    simio_create_ucontext(pcr);
    *dpi_id = pcr->dpi_id;
    
    printf("C: simio_init:pcr->dpi_id=%08x, *dpi_id=%08x\n", pcr->dpi_id, dpi_id[0]); fflush(NULL);
}

void
simio_close(const svBitVecVal *dpi_id)
{
    coroutine_t *pcr = simio_coroutine(*dpi_id);
    unsigned     dbg = 0;

    if (dbg)
        simio_dump(*dpi_id);
    free(pcr);
    printf("C: simio_close:\n");fflush(NULL);
    // XXX fixup the crlist.
}
