#ifndef SIMIO_PUBLIC_HEADER_H
#define SIMIO_PUBLIC_HEADER_H

#include <stdlib.h>
#include <stdint.h>
#include <svdpi.h>

#define SIMIO_CMD_XCHG          1
#define SIMIO_CMD_FINISH        2
#define SIMIO_CMD_ERROR         3

// public functions
extern void      simio_yield_v2c(const svBitVecVal *dpi_id);
extern void      simio_yield_c2v(const uint32_t dpi_id, const uint32_t dpi_cmd);
extern uint32_t *simio_shmem_c2v(const uint32_t dpi_id);
extern uint32_t *simio_shmem_v2c(const uint32_t dpi_id);
extern void      simio_shmem(const svBitVecVal *dpi_id, const svBitVecVal *v2c, svBitVecVal *dpi_cmd, svBitVecVal *c2v);
extern void      simio_open(svBitVecVal *dpi_cfg, svBitVecVal *dpi_id);
extern void      simio_close(const svBitVecVal *dpi_id);
extern void      simio_dump(const uint32_t dpi_id);

#endif
